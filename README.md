# tablejs
Create tables from js objects with vanilla javascript.

## Dynamically Create Table

#### Demo
```javascript
// for other formats see "Data Formats" section
const data_obj = [
  {a: 0, b: 1, c: '/link1'},
  {a: 2, b: 3, c: '/link2'},
  {a: 4, b: 5, c: '/link3'},
]

const table_element = table_builder(data_obj, {
  // only display columns 'c' and 'a' in that order
  display: ['c', 'a'],

  // headers displayed as uppercase letters
  modify_headers: h => h.toUpperCase(),

  // create a hyperlink from c
  modify_headers: {c: link => `<a href="${link}"><p>Download</p></a>`},
})
```

**table_builder**(data, options)  
Returns a table element that contains a header and body that defined by data.  
  
data - Array or object with a format shown in DataAccess  
  
**options** (optional) - Object  
  options.**display**  
    Defines the columns to display and their order.  
    If an array of strings those are displayed in that order.  
    If undefined or 'all' all data columns will be shown.  
  
  options.**modify_headers**  
    Function that modifies the header strings.  
    Function: head_label => HTML_String  
    Does not need to be defined.  
  
  options.**modify_data**  
    Object that contains functions to modify the content of all the data in displayed in a column.  
    Key specifies the column to be modified.  
    Value: cell_text => HTML_String  
    Keys do not need to be defined for all header labels.  



## Data Formats
Allows 3 different data formats.  
#### Object Format
```javascript
[
  {a: 0, b: 1},
  {a: 2, b: 3},
  {a: 4, b: 5},
]
```
#### Row Format
```javascript
{
  head: ['a', 'b'],
  rows: [
    [0, 1],
    [2, 3],
    [4, 5],
  ],
}
```
#### Column Format
```javascript
{
  head: ['a', 'b'],
  columns: [
    [0, 2, 4],
    [1, 3, 5],
  ],
}
```

## testing
Using karma, mocha, chai, headless chrome.  
Run tests with `npm test`  
Reference: https://developers.google.com/web/updates/2017/06/headless-karma-mocha-chai  

