'use strict'

/*global describe it chai expect table_builder DataAccess */

/*eslint no-unused-expressions: 'off' */ // for chai
chai.should()


describe('Building tables from js object', function() {
  describe('by row.', function() {
    describe('Creates just a', function() {
      it('header and rows.', function() {
        const data_obj = {
          head: ['a', 'b'],
          rows: [
            [1, 2],
            [3, 4],
          ],
        }

        const table_ref = ` <table>
                              <thead>
                                <tr>
                                  <th>a</th>
                                  <th>b</th>
                                </tr>
                                <tbody>
                                  <tr>
                                    <td>1</td>
                                    <td>2</td>
                                  </tr>
                                  <tr>
                                    <td>3</td>
                                    <td>4</td>
                                  </tr>
                                </tbody>
                              </thead>
                            </table>`.htmlToElement()

        const table = table_builder(data_obj)

        table.isEqualNode(table_ref).should.be.true
      })
    })

    describe('Select/Order specific columns.', function() {
      it('Displays only 2 columns when 3 data columns.', function() {
        const data_obj = {
          head: ['a', 'b', 'c'],
          columns: [
            [1, 4],
            [2, 5],
            [3, 6],
          ],
        }

        const table_ref = ` <table>
                              <thead>
                                <tr>
                                  <th>a</th>
                                  <th>b</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>1</td>
                                  <td>2</td>
                                </tr>
                                <tr>
                                  <td>4</td>
                                  <td>5</td>
                                </tr>
                              </tbody>
                            </table>`.htmlToElement()

        const table = table_builder(data_obj, {display: ['a', 'b']})

        table.isEqualNode(table_ref).should.be.true
      })

      it('Displays columns in correct order.', function() {
        const data_obj = [
          {a: 1, b: 2, c: 3},
          {a: 4, b: 5, c: 6},
        ]

        const table_ref = ` <table>
                              <thead>
                                <tr>
                                  <th>c</th>
                                  <th>a</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>3</td>
                                  <td>1</td>
                                </tr>
                                <tr>
                                  <td>6</td>
                                  <td>4</td>
                                </tr>
                              </tbody>
                            </table>`.htmlToElement()

        const table = table_builder(data_obj, {display: ['c', 'a']})

        table.isEqualNode(table_ref).should.be.true
      })

    })

    describe('Modify the headers', function() {
      it('to uppercase.', function() {
        const data_obj = {
          head: ['a', 'b'],
          rows: [],
        }

        const table_ref = ` <table>
                              <thead>
                                <tr>
                                  <th>A</th>
                                  <th>B</th>
                                </tr>
                              </thead>
                              <tbody></tbody>
                            </table>`.htmlToElement()

        const table = table_builder(
          data_obj,
          {modify_headers: h => h.toUpperCase()}
        )

        table.isEqualNode(table_ref).should.be.true
      })
    })

    describe('Modify the body values', function() {
      it('multiplying "a" column by 2', function() {
        const data_obj = {
          head: ['a', 'b'],
          rows: [
            [1, 3],
            [5, 7],
          ],
        }

        const table_ref = ` <table>
                              <thead>
                                <tr>
                                  <th>a</th>
                                  <th>b</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>2</td>
                                  <td>3</td>
                                </tr>
                                <tr>
                                  <td>10</td>
                                  <td>7</td>
                                </tr>
                              </tbody>
                            </table>`.htmlToElement()

        const table = table_builder(
          data_obj,
          {modify_data: {a: x => 2 * x}}
        )

        table.isEqualNode(table_ref).should.be.true
      })
    })

  })

})

/*eslint no-extend-native: ["error", { "exceptions": ["String"] }]*/
String.prototype.htmlToElement = function () {
  /*
  Takes an html string and converts it to elements and returns
  the parent element.
  LIMITATION: Must have only one top level parent element.
  Reference: https://stackoverflow.com/a/494348/6933270

  html - string that contains html
  */
  const container = document.createElement('_')
  container.innerHTML = this.replace(/ |\n/g, '')

  if (container.children.length !== 1)
    throw new Error(`html defines ${container.children.length} outer elements but should only define 1.`)

  return container.lastChild
}


describe('DataAccess.', function() {
  describe('Header and body with data rows.', function() {
    it('Expected case.', function() {
      const data_obj = {
        head: ['a', 'b'],
        rows: [
          [1, 2],
          [3, 4],
          [5, 6],
        ],
      }

      const ref_head = ['a', 'b'],
            ref_length = 3,
            ref_data = [
              {a: 1, b: 2},
              {a: 3, b: 4},
              {a: 5, b: 6},
            ]

      const data_access = DataAccess(data_obj)

      validate_data(data_access, ref_data)

      expect(data_access.head).to.eql(ref_head)
      expect(data_access.len).to.equal(ref_length)
    })
  })

  describe('Header and body with data columns.', function() {
    it('Expected case.', function() {
      const data_obj = {
        head: ['c', 'd'],
        columns: [
          [5, 6, 7],
          [8, 9, 0],
        ],
      }

      const ref_head = ['c', 'd'],
            ref_length = 3,
            ref_data = [
              {c: 5, d: 8},
              {c: 6, d: 9},
              {c: 7, d: 0},
            ]

      const data_access = DataAccess(data_obj)

      validate_data(data_access, ref_data)

      expect(data_access.head).to.eql(ref_head)
      expect(data_access.len).to.equal(ref_length)
    })
  })

  describe('Array of objects.', function() {
    it('Expected case.', function() {
      const data_obj = [
        {e: 0, f: 1},
        {e: 2, f: 3},
      ]
      const ref_head = ['e', 'f'],
            ref_length = 2,
            ref_data = [
              {e: 0, f: 1},
              {e: 2, f: 3},
            ]

      const data_access = DataAccess(data_obj)

      validate_data(data_access, ref_data)

      expect(data_access.head).to.eql(ref_head)
      expect(data_access.len).to.equal(ref_length)
    })
  })

  describe('Ensure errors thrown.', function() {
    it('No data.head.', function() {
      expect(function() {
        table_builder({head: undefined, rows: [], columns: []})
      }).to.throw('Unknown data fromat.')
    })
    it('No data.rows or data.columns.', function() {
      expect(function() {
        table_builder({head: [], rows: undefined, columns: undefined})
      }).to.throw('Unknown data fromat.')
    })

  })

  function validate_data(data_access, reference) {
    for (let row = reference.length - 1; row >= 0; --row) {
      for (const column in reference[row]) {
        if (reference[row].hasOwnProperty(column)) {
          expect(data_access(column, row), `column: ${column}, row: ${row}`)
            .to.equal(reference[row][column])
        }
      }
    }
  }
})
