'use strict'
/* exported table_builder DataAccess */

/*
Returns a table element that contains a header and body that defined by data.

data - Array or object with a format shown in DataAccess

options (optional) - Object
  options.display - Defines the columns to display and their order.
                    If an array of strings those are displayed in that order.
                    If undefined or 'all' all data columns will be shown.
  options.modify_headers - Function that modifies the header strings.
                           Function: head_label => HTML_String
                           Does not need to be defined.
  options.modify_data - Object that contains functions to modify the content
                        of all the data in displayed in a column.
                        Key specifies the column to be modified.
                        Value: cell_text => HTML_String
                        Keys do not need to be defined for all header labels.
*/
function table_builder(
  data,
  // options
  {
    display = 'all',
    modify_headers = x=>x,
    modify_data = {},
  } = {}
) {

  const data_access = DataAccess(data)

  if (display === 'all') display = data_access.head

  // set default modify functions
  const pass_val = x=>x
  display.forEach(head_label => {
    if (modify_data[head_label] === undefined)
      modify_data[head_label] = pass_val
  })

  /* build the table, finally */
  const table = document.createElement('table')

  const thead = createAppendElement(table, 'thead'),
        tr = createAppendElement(thead, 'tr')
  display.forEach(head_label => {
    createAppendElement(tr, 'th').innerHTML = modify_headers(head_label)
  })

  const tbody = createAppendElement(table, 'tbody')
  for (let row = 0; row < data_access.len; ++row) {
    const tr = createAppendElement(tbody, 'tr')

    display.forEach(head_label => {
      createAppendElement(tr, 'td').innerHTML =
        modify_data[head_label](data_access(head_label, row))
    })
  }

  return table

  function createAppendElement(parent, tag) {
    /*
    Creates a new element of type tag.
    Appends new element to parent.
    Returns new element.

    parent - element that the new element is appended to
    tag - string that defines the new element's tag
    */
    const new_element = document.createElement(tag)
    parent.appendChild(new_element)
    return new_element
  }

}


function DataAccess(data) {
  /*
  DataAccess.head - Array of head strings
  DataAccess.len - Number of rows
  */

  /* Object Format
  [
    {a: 0, b: 1},
    {a: 2, b: 3},
    {a: 4, b: 5},
  ]
  */
  if (Array.isArray(data)) {
    const da_func = function(label, row_index) {
      return data[row_index][label]
    }
    da_func.writable = true
    da_func.head = data.length > 0 ? Object.keys(data[0]) : undefined
    da_func.len = data.length

    return da_func
  }

  const {head, rows, columns} = data

  /* Row Format
  {
    head: ['a', 'b'],
    rows: [
      [0, 1],
      [2, 3],
      [4, 5],
    ],
  }
  */
  if (head !== undefined && rows !== undefined) {
    const da_func = function(label, row_index) {
      return rows[row_index][head.indexOf(label)]
    }
    da_func.writable = true
    da_func.head = head
    da_func.len = rows.length

    return da_func
  }

  /* Column Format
  {
    head: ['a', 'b'],
    columns: [
      [0, 2, 4],
      [1, 3, 5],
    ],
  }
  */
  if (head !== undefined && columns !== undefined) {
    const da_func = function(label, row_index) {
      return columns[head.indexOf(label)][row_index]
    }
    da_func.writable = true
    da_func.head = head
    da_func.len = columns.length > 0 ? columns[0].length : undefined

    return da_func
  }

  throw new Error('Unknown data fromat.')
}

